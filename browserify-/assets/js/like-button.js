const Like = require("./like");
const Unlike = require("./unlike");

class LikeButton extends React.Component {
        constructor(props) {
            super(props);
            this.state = { liked: false };
        }

        toggleLike(event) {
            event.preventDefault();

            let toggledLike = !this.state.liked;
            this.setState({ liked: toggledLike })

            return toggledLike;
        }

        render() {
            if (this.state.liked) {
                return ( <button className = "btn btn-unlike"
                    onClick = { this.toggleLike.bind(this) } >
                    <Unlike/>
                    </button> );
                }

            return ( <button className = "btn btn-like"
                    onClick = { this.toggleLike.bind(this) }>
                    <Like />
                    </button>);

        } // render
} // LikeButton

const domContainer = document.querySelector('#like_button_container');
ReactDOM.render( <LikeButton/> , domContainer);