## Description
---
By Weng Fei Fung. React setup using browserify which is a node CLI to bundle files. It is much easier to use than webpack. Requires node and package.json, and the package.json will have a `build` script that calls browserify. The downside to browserify is you have to add the script filenames to the browserify command line, so it can get tedious the more components you have.

## Explanation
At your index.html, notice that you have to load the remote js scripts React and React-DOM. React is the core React engine. React-DOM is in charge of rendering to the web browser. They keep it separate because there's another library for rendering to mobile which is React Native, so they want the developer to be explicit what they're rendering to.

If you choose to install React and React-DOM using npm, you can omit these script tags then make sure to require to React and React-DOM on the highest component. You do not need to require it at every descendant component since the newest React version.

If first time using browserify, install it globally:
```
npm install -g browserify
```

Babel transpile or convert the JSX syntax to plain JS for browsers to understand. Install Babel, its dependencies, and presets:
```
npm install babel-cli babel-preset-env babel-preset-react babelify@8 --save-dev
```
Then add browserify to a `build` script at package.json. If it does not run, you can precede it with npx to bypass having to add browserify path to $PATH env variable. Notice the options tells browserify what to bundle and from what language.
```
"scripts": {
    "build": "browserify -t [ babelify --presets [ react ] ] ./assets/js/like-button.js ./assets/js/like.js ./assets/js/unlike.js -o ./dist/js/bundle.min.js"
},
```

Add configuration for Babel so it knows you are converting jsx to js. Create the file and use this boilerplate.
```
// .babelrc:
{
  "presets": ["env", "react"]
}
```

Then you can:
```
npm run build
```

## How ES modules work
Nowadays in 2021, web browsers can bundle ES modules for you but they cannot bundle ES modules that have JSX because of their specs requiring strict JS for modules. So you'd still have to use a bundler like browserify or webpack. If you do not want to use a bundler, then you have to load all the components flat at index.html like in the [plain JSX setup](./jsx.md).

ES modules follow this format. The module can import dependencies with:
```
//App.js:
import Like from './like.js';

```

The module that's dependency must `export default`:
```
export default class Icon extends React.Component {
	constructor(props) {
		super(props);
	}
	render() {
	// ... jsx or plain js using React.createElement
	}
} // Component class
```

## Explanation of folder name.
Notice the folder name is `webpack-` with an extra hyphen at the end. If you are naming a folder, it's good practice to always make sure it's unique from the modules you may be using because it can cause errors.