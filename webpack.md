## Description
---
By Weng Fei Fung. React setup using webpack. Requires node and package.json. Webpack will be setup with its configurations at webpack.config.js, then package.json will have a `build` script that calls webpack

## Explanation
At your index.html, notice that you have to load the remote js scripts React and React-DOM. React is the core React engine. React-DOM is in charge of rendering to the web browser. They keep it separate because there's another library for rendering to mobile which is React Native, so they want the developer to be explicit what they're rendering to.

If you choose to install React and React-DOM using npm, you can omit these script tags then make sure to require to React and React-DOM on the highest component. You do not need to require it at every descendant component since the newest React version.

Next install webpack which will bundle all the component js files into one js file that browsers can understand. In addition, it will transpile or convert the JSX syntax to plain JS for browsers. Webpack is really a file operation tool for React:
```
npm install --save-dev webpack webpack-cli
```

Babel transpile or convert the JSX syntax to plain JS for browsers to understand. Install Babel, its dependencies, and presets:
```
npm install --save-dev @babel/core
npm install --save-dev babel-loader
npm i @babel/preset-react @babel/preset-env @babel/plugin-transform-react-jsx @babel/plugin-syntax-jsx -D
```

Then add webpack to a `build` script at package.json. Notice that I preceded with npx. This is because on some people's computers, installing webpack has problems installing the command-line even if they installed globally. Instead of editing the $PATH env variable, npx will run the command as is for the npm module.
```
// package.json:
  "scripts": {
      "build": "npx webpack"
  }
```

Add configuration for webpack. Create the file and use this boilerplate. Pay attention to the paths
```
// webpack.config.json:
const path = require('path');
 
module.exports = {
  entry: path.resolve(__dirname, './assets/js/like-button.js'),
  mode: "production",
  module: {
    rules: [
      {
        test: /\.(js)$/,
        exclude: /node_modules/,
        use: ['babel-loader']
      }
    ]
  },
  resolve: {
    extensions: ['*', '.js']
  },
  output: {
    path: path.resolve(__dirname, './dist/js'),
    filename: 'bundle.min.js',
  },
  devServer: {
    contentBase: path.resolve(__dirname, './'),
  },
};
```

Add configuration for Babel so it knows you are converting jsx to js. Create the file and use this boilerplate.
```
// .babelrc:
{
    "presets": [
      "@babel/preset-env"
    ],
    "plugins": ["@babel/plugin-transform-react-jsx"]
}
```

You transpile with:
```
npm run build
```

## Troubleshooting: Multiple configuration files found. Please remove one?
You might have followed another tutorial that added babel configurations at the package.json. Remember you just added it using the .babelrc method. There are in fact multiple ways to configure babel. You can remove the babel entry in package.json if this is the case. For example, this tutorial asks you to add babel configuration in package.json: https://www.robinwieruch.de/webpack-babel-setup-tutorial

## How ES modules work
Nowadays in 2020, web browsers can bundle ES modules for you but they cannot bundle ES modules that have JSX because of their specs requiring strict JS for modules. So you'd still have to use a bundler like browserify or webpack. If you do not want to use a bundler, then you have to load all the components flat at index.html like in the [plain JSX setup](./jsx.md).

ES modules follow this format. The module can import dependencies with:
```
//App.js:
import Like from './like.js';

```

The module that's dependency must `export default`:
```
export default class Icon extends React.Component {
	constructor(props) {
		super(props);
	}
	render() {
	// ... jsx or plain js using React.createElement
	}
} // Component class
```

## Explanation of folder name.
Notice the folder name is `webpack-` with an extra hyphen at the end. If you are naming a folder, it's good practice to always make sure it's unique from the modules you may be using because it can cause errors.