## Description
---
By Weng Fei Fung. React setup using just plain javascript. No node necessary. The components use plain javascript and no JSX. Remember that JSX is optional in React.

## Explanation
At your index.html, notice that you have to load the remote js scripts React and React-DOM. React is the core React engine. React-DOM is in charge of rendering to the web browser. They keep it separate because there's another library for rendering to mobile which is React Native, so they want the developer to be explicit what they're rendering to. By loading these React js assets, you do not have to install React npm modules.

The index.html loads the highest local js component. All other components are its descendents.
```
<script type="module" src="assets/js/like-button.js"></script>
```

^ The important attribute here is `type="module"` which tells the browser to treat the JS file as an ES module and to pay attention to keywords like import. In like-button .js, you have the line `import Like from './like.js';`, so the web browser will include that component `like.js` as a dependency into `like-button.js`.

Besides the highest module, all modules must be explicit what is being exported. Notice in `like.js` there is this line:
```
export default class Icon...
```

If you have other modules as dependencies in like-button.js or in like.js, you follow a similar pattern. You import the dependency, and at the dependency you must export itself. To summarize, the two important lines are:
```
import Like from './like.js'; // at the module that needs to use dependencies
export default class Icon; // at the module that's the dependency
```

## Usage
You can run as a static website because no node necessary. Install "Open in browser" extension for Visual Code, right-click file, click "Open in default browser".