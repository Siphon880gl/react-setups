import Like from './like.js';
const cre = React.createElement;

class LikeButton extends React.Component {
    constructor(props) {
        super(props);
        this.state = { liked: false };
    }

    render() {
        if (this.state.liked) {
            return cre("span", {},
                cre('span', {}, `You liked this. `,
                    cre('a', { href: "#", onClick: () => this.setState({ liked: false }) }, `Unlike`)
                )
            );
        }

        return cre(
            'button', {
                className: "btn btn-like",
                onClick: () => this.setState({ liked: true })
            },
            cre(
                Like
            )
        );
    }
}

const domContainer = document.querySelector('#like_button_container');
ReactDOM.render(cre(LikeButton), domContainer);