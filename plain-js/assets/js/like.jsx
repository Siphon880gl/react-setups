const cre = React.createElement;

export default class Icon extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return cre(
            'i', { className: "fa fa-thumbs-up" }, 'Like'
        );
    }
}