React Setups
====
<a href="https://github.com/Siphon880gh/" rel="nofollow" target="_blank"><img class="my-badge" src="https://img.shields.io/badge/Github-darkgray?style=flat&amp;logo=github&amp;labelColor=darkgray&amp;logoColor=white" alt="Youtube" data-canonical-src="https://img.shields.io/badge/Github-darkgray?style=flat&amp;logo=youtube&amp;labelColor=lightgray&amp;labelColor=white"></a> <a target="_blank" href="https://www.linkedin.com/in/weng-fung/" rel="nofollow"><img src="https://img.shields.io/badge/LinkedIn-blue?style=flat&logo=linkedin&labelColor=blue" alt="Linked-In" data-canonical-src="https://img.shields.io/badge/LinkedIn-blue?style=flat&amp;logo=linkedin&amp;labelColor=blue" style="max-width:100%;"></a> <a target="_blank" href="https://www.youtube.com/user/Siphon880yt/" rel="nofollow"><img src="https://img.shields.io/badge/Youtube-red?style=flat&logo=youtube&labelColor=red" alt="Youtube" data-canonical-src="https://img.shields.io/badge/Youtube-red?style=flat&amp;logo=youtube&amp;labelColor=red" style="max-width:100%;"></a> <a target="_blank" href="https://www.paypal.com/donate?business=T42BK25TYPZSA&item_name=Buy+me+coffee+%28I+develop+free+apps%29&currency_code=USD" title="Donate to this project using Buy Me A Coffee" alt="Paypal"><img src="https://img.shields.io/badge/buy%20me%20a%20coffee-donate-yellow.svg" alt="Buy Me A Coffee donate button" /></a>

Description
---
By Weng Fei Fung. Different ways to setup React: Plain javascript, Plain JSX, Browserify, Webpack, Create-react-app. Going through these different setups will also teach you some intricacies of React, Babel, and JSX.

Table of Contents
---
- [Description](#description)
- [Installation](#installation)
- [Questions](#questions)

Installation
---
Run `npm install` at each folder to install their npm modules if applicable. Some setups do not use npm modules.

Read how to setup React:
- [Create-react-app (Easiest, Most performant)](./create-react-app.md)
- [Browserify (Next Easiest)](./browserify.md)
- [Webpack (Hardest)](./webpack.md)
- [Plain javascript (Less performant)](./plain-js.md)
- [Plain JSX (Least performant)](./jsx.md)

Questions
---
- Where can I reach you?
	- You can reach me with additional questions at <a href='mailto:siphon880g@gmail.com'>siphon880g@gmail.com</a>.
	- Want to [hire me](https://www.linkedin.com/in/weng-fung/)?