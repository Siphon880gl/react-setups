## Description
---
By Weng Fei Fung. React setup using create-react-app. Requires node and package.json. The easiest method and the most performant setup. Webpack is used under the hood but we never touch it. The create-react-app CLI tool creates a new folder for the project, and inside that folder has a package.json with multiple scripts. The create-react-app also installs another CLI tool `react-scripts` that your npm scripts at package.json will use to run the page among other things. If you run `npm run eject`, you can eject the code from the hood and have access to webpack etc that was working underneath.

## Instructions
If first time with create-react-app, you want to install it globally
```
npm install -g create-react-app my-app
```

Create app. Notice the last argument. You can change that to your project name:
```
npx create-react-app my-app
```

Cd into my-app, then build:
```
cd my-app
npm run build
```

After building, the console will tell you how to serve the page into your web browser, much like:
```
serve -s build
```

You can serve the page with that command and it'll open the web browser to the React page.

## Explanation of start script
This is a node server that gets started with `npm run start`. The start script is the command-line `react-scripts start`. The `react-scripts` is a CLI tool that create-react-app installed to help it setup your page.

## Explanation of folder name.
Notice the folder name is `create-react-app-` with an extra hyphen at the end. If you are naming a folder, it's good practice to always make sure it's unique from the modules you may be using because it can cause errors. In this case, it's going to create package.json inside another folder so it wouldn't cause problems.