## Description
---
By Weng Fei Fung. React setup using just JSX. No node necessary.

## Explanation
At your index.html, notice that you have to load the remote js scripts React and React-DOM. React is the core React engine. React-DOM is in charge of rendering to the web browser. They keep it separate because there's another library for rendering to mobile which is React Native, so they want the developer to be explicit what they're rendering to. By loading these React js assets, you do not have to install React npm modules.

You may remember that JSX is not natively understood by the web browser so you'd need a transpiler like Babel that converts JSX to plain JS. You still do not need node to run Babel. Notice at index.html, we can load the remote js script babel:
```
<script src="https://cdnjs.cloudflare.com/ajax/libs/babel-standalone/6.14.0/babel.min.js"></script>
<script type="text/babel" data-presets="es2017, stage-3" data-plugins="syntax-async-functions,transform-class-properties"></script>
```

### Side note: We cannot use import with jsx syntax

You may want to use some language like
```
    <script src="https://cdn.jsdelivr.net/npm/@babel/standalone@7.13.9/babel.min.js" crossorigin></script>
    <script type="text/babel" data-type="module" src="assets/js/like-button.jsx"></script>
```

Then at like-button, you import like.jsx and unlike.jsx. Unfortunately you cannot unless you use Service Worker to fetch thoes files and convert them to plain JS before they get imported. You cannot natively transpile the modules then bundle them natively without service workers intercepting the asset files. You'll run into an error stating that plain javascript is required in ES modules:

    unlike.jsx:1 Failed to load module script: The server responded with a non-JavaScript MIME type of "text/plain". Strict MIME type checking is enforced for module scripts per HTML spec.


### Continuing explanation


The index.html loads all the jsx components. This is the consequence of the side note. 

```
    <script type="text/babel" src="assets/js/like.jsx"></script>
    <script type="text/babel" src="assets/js/unlike.jsx"></script>
    <script type="text/babel" src="assets/js/like-button.jsx"></script>
```

You can make a comment above each component file what they depend on if it helps with readability. For example, above like-button.js could be:
```
// Depends on: Like from like.jsx
// Depends on: Unlike from unlike.jsx
```

## Usage
You can run as a static website because no node necessary. Install "Open in browser" extension for Visual Code, right-click file, click "Open in default browser".